package com.tora;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Calculator {

    private Double total = 0.0;

    public Double getTotal() {
        return total;
    }

    public void run() {
        try{
            while (true){
                Double currValue;
                Scanner scanner = new Scanner(System.in);

                String operator = scanner.nextLine();
                currValue = scanner.nextDouble();

                this.op(operator, currValue);

                System.out.println("new total: " + total);
            }
        } catch (InputMismatchException er)
        {
            System.out.println("Final total: " + total);
            System.out.println("Exiting...");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void op(String operator, Double currValue) throws Exception {
        switch (operator) {
            case "+" -> {
                total += currValue;
            }
            case "-" -> {
                total -= currValue;
            }
            case "/" -> {
                if (currValue == 0) {
                    System.out.println("0 is not a valid value\n");
                    throw new Exception("0 is not a valid value\n");
                }
                total /= currValue;
            }
            case "*" -> {
                total *= currValue;
            }
            default -> {
                throw new InputMismatchException();
            }
        }
    }

}


