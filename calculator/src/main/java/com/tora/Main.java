package com.tora;


public class Main {
    public static void main(String[] args) {
        System.out.println("Starting calculator...");
        Calculator calculator = new Calculator();
        calculator.run();
    }
}