import com.tora.Calculator;
import org.junit.Before;
import org.junit.Test;

import java.util.InputMismatchException;

import static org.junit.Assert.*;

public class CalculatorTest {

    public Calculator calculator;

    @Before
    public void setup() {
        calculator = new Calculator();
    }

    @Test
    public void testTotalGetter() {
        assertEquals(calculator.getTotal(), 0.0, 0.001);
    }

    @Test
    public void testAddition() {
        try {
            calculator.op("+", 12.0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            fail();
        }
        assertEquals(calculator.getTotal(), 12.0, 0.001);
    }

    @Test
    public void testSubtraction() {
        try {
            calculator.op("-", 12.0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            fail();
        }
        assertEquals(calculator.getTotal(), -12.0, 0.001);
    }

    @Test
    public void testDivision() {
        try {
            calculator.op("+", 12.0);
            calculator.op("/", 4.0);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            fail();
        }
        assertEquals(calculator.getTotal(), 3.0, 0.001);
    }

    @Test
    public void testDivisionWithZero() {
        try {
            calculator.op("+", 12.0);
            calculator.op("/", 0.0);
        }catch (Exception e)
        {
            assertEquals(e.getMessage(), "0 is not a valid value\n");
        }
    }

    @Test
    public void testMultiplication() {
        try {
            calculator.op("+", 12.0);
            calculator.op("*", 4.0);
        }catch (Exception e) {
            System.out.println(e.getMessage());
            fail();
        }
        assertEquals(calculator.getTotal(), 48.0, 0.001);
    }

    @Test
    public void testDefault() {
        try{
            calculator.op("x", 12.0);
        }catch (InputMismatchException er)
        {
            assertTrue(true);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            fail();
        }
    }

}
